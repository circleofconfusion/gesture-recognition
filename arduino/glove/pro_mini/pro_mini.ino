#include <XBee.h>
#include <SoftwareSerial.h>
#include <SoftEasyTransfer.h>

enum Gestures {
    NULL_GESTURE,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    OK,
    FREEZE,
    I_HEAR,
    I_SEE,
    ENEMY,
    HOSTAGE
};

enum Motors {
    THUMB=2,
    INDEX,
    MIDDLE,
    RING,
    PINKY,
    FIRST_DORSAL_INTEROSSEOUS
};

XBee xbee = XBee();
ZBRxResponse zbRx = ZBRxResponse();

// software serial constants
const int RX = 12;
const int TX = 11;
SoftwareSerial sSerial (RX,TX);

SoftEasyTransfer ET;

struct TRANSFER_STRUCT {
    byte data[36];
};

TRANSFER_STRUCT rData;

const char UNIT_ID[2] = {'R', 'H'};

// RFS
int thumbFlex =  A0;
int indexFlex =  A1;
int middleFlex = A2;
int ringFlex =   A3;
int pinkyFlex =  A4;
int thumbVal;
int indexVal;
int middleVal;
int ringVal;
int pinkyVal;

union ShortSplit {
    short s;
    byte b[2];
};

void setup() {
  // initialize serial comm
  sSerial.begin(57600);
  ET.begin(details(rData), &sSerial);
  Serial.begin(57600);
  xbee.begin(Serial);

  pinMode(THUMB, OUTPUT);
  pinMode(INDEX, OUTPUT);
  pinMode(MIDDLE, OUTPUT);
  pinMode(RING, OUTPUT);
  pinMode(PINKY, OUTPUT);
  pinMode(FIRST_DORSAL_INTEROSSEOUS, OUTPUT);
}

void loop() {

    // Read data incoming from attached Razor IMU
    if (ET.receiveData()) {
        // gather flex sensor data
        thumbVal = analogRead(thumbFlex);
        indexVal = analogRead(indexFlex);
        middleVal = analogRead(middleFlex);
        ringVal = analogRead(ringFlex);
        pinkyVal = analogRead(pinkyFlex);
    
        ShortSplit ssT;
        ShortSplit ssI;
        ShortSplit ssM;
        ShortSplit ssR;
        ShortSplit ssP;
    
        ssT.s = thumbVal;
        ssI.s = indexVal;
        ssM.s = middleVal;
        ssR.s = ringVal;
        ssP.s = pinkyVal;
        
        // assemble the sensor data into one of bytes
        byte payload[48] = {
            UNIT_ID[0], UNIT_ID[1],
            rData.data[0], rData.data[1], rData.data[2], rData.data[3],
            rData.data[4], rData.data[5], rData.data[6], rData.data[7],
            rData.data[8], rData.data[9], rData.data[10], rData.data[11],
            rData.data[12], rData.data[13], rData.data[14], rData.data[15],
            rData.data[16], rData.data[17], rData.data[18], rData.data[19],
            rData.data[20], rData.data[21], rData.data[22], rData.data[23],
            rData.data[24], rData.data[25], rData.data[26], rData.data[27],
            rData.data[28], rData.data[29], rData.data[30], rData.data[31],
            rData.data[32], rData.data[33], rData.data[34], rData.data[35],
            ssT.b[0], ssT.b[1],
            ssI.b[0], ssI.b[1],
            ssM.b[0], ssM.b[1],
            ssR.b[0], ssR.b[1],
            ssP.b[0], ssP.b[1]
        };
        
        XBeeAddress64 contAddr64 = XBeeAddress64(0x0, 0x0);
        ZBTxRequest zbTx = ZBTxRequest(contAddr64, payload, sizeof(payload));
        ZBTxStatusResponse txStatus = ZBTxStatusResponse(); 
        
        xbee.send(zbTx);
    }

    // Read tactile commands coming from controller
    xbee.readPacket();

    if (xbee.getResponse().isAvailable()) {
        if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
            xbee.getResponse().getZBRxResponse(zbRx);

            switch (zbRx.getData(0)) {

                case ONE:
                    digitalWrite(INDEX, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    break;

                case TWO:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    break;

                case THREE:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(THUMB, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(THUMB, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(THUMB, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(THUMB, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(THUMB, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(THUMB, LOW);
                    break;

                case FOUR: 
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    break;

                case FIVE:
                    digitalWrite(THUMB, HIGH); 
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    break;

                case SIX:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    break;

                case SEVEN:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(PINKY, LOW);
                    break;

                case EIGHT:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    break;

                case NINE:
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    break;

                case TEN:
                    digitalWrite(THUMB, HIGH); 
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    break;

                case OK:
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(50);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(100);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(50);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);

                case FREEZE:
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);

                case I_HEAR:
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);

                case I_SEE:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);

                case ENEMY:
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    delay(50);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(MIDDLE, HIGH);
                    digitalWrite(RING, HIGH);
                    digitalWrite(PINKY, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);

                case HOSTAGE:
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
                    delay(50);
                    digitalWrite(THUMB, HIGH);
                    digitalWrite(INDEX, HIGH);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, HIGH);
                    delay(100);
                    digitalWrite(THUMB, LOW);
                    digitalWrite(INDEX, LOW);
                    digitalWrite(MIDDLE, LOW);
                    digitalWrite(RING, LOW);
                    digitalWrite(PINKY, LOW);
                    digitalWrite(FIRST_DORSAL_INTEROSSEOUS, LOW);
            }
        }
    }
    
    delay(10);
}
