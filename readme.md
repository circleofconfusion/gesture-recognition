# Gesture Recognition Glove Code

This code is divided up into two parts, Arduino/C++ code for the glove 
hardware, and C++/QT5 code for managing the incoming data from the 
glove, recognizing gestures, and sending commands to the glove.

In theory, this code can run on Mac OSX, but it has not been tested on that
platform because I don't have access to such a machine. 

As of January 2017, GRT did not build successfully on Windows, though it is
being worked on.

# Dependencies

## Ubuntu
- libudev-dev
- cmake
- build-essentials
- QT 5.6 or higher

# Building on Ubuntu Linux

## Building GRT

The first step is to build the Gesture Recognition Toolkit so that its library
can be included in the rest of the project.

First, you'll need these dependencies to build it:

    sudo apt install build-essential cmake libudev-dev

Download or clone GRT from the [GitHub page:](https://github.com/nickgillian/grt)
Then follow the instructions for building in the build/Readme.md file wherever
you unzipped GRT. I recommend following the suggested instructions, but settings
like where the GRT library is installed, but see below in the QT section if you
install to anywhere other than /usr/local

## Building the QT GUI

To build the gesture recognition, signal sending, and GUI code, you'll first
need QT to be installed somewhere on your system. The QT libraries in the
Ubuntu repository are usually outdated, so instead download and install them
from the QT [download page](https://www.qt.io/download/). I chose to install 
to the /opt/ directory, but these could, in theory, be installed anywhere.

This will take some time to both download and install QT.

Once finished, open QT Creator, and open GestureReader/SerialMonitor/SerialMonitor.pro
found where you cloned this code. If you installed GRT to /usr/local, you
should be able to simply build and run the GUI using the Ctrl+R keyboard shortcut,
or by clicking the Run icon.

If you installed GRT to another directory, you'll need to update SerialMonitor.pro
to point to that directory instead of /usr/local.

# Uploading Compiled Code to the Gloves

The gloves are composed of two boards. The smaller blue board is an Arduino Pro Mini
3 volts 8Mhz. The larger red board is a Sparkfun Razor IMU. This board contains the
exact same microcontroller as the Arduino Pro Mini, and has an accelerometer,
gyroscope, and magnetometer built into the board.

Existing code can be found in `arduino/glove/pro_mini/pro_mini.ino` and
`arduino/glove/razor/razor.ino`.

Both units have a 6-pin header on
one end that is designed to be attached to a FTDI 3 volt cable. *It is extremely
important that you use a 3 volt cable - 5 volts will fry the boards.* Alternatively,
you can use a FTDI breakout board like
[this one from DFRobot](https://www.amazon.com/gp/product/B01879AYQK/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1).
Just be sure to set the voltage select jumper to 3 volts before attaching to either
the Arduino or Razor IMU.

Compiled code is uploaded to the Arduino and Razor IMU using the Arduino IDE, which
can be [downloaded here](https://www.arduino.cc/en/Main/Software). After you have
made your code changes, you'll need to set up the IDE to compile for the specific
boards you're using. Click the Tools menu, mouse over Board and select
"Arduino Pro or Pro Mini". Next, click Tools again, mouse over Processor, and
select "ATMega328 (3.3V, 8 Mhz)". Last, select the serial port your FTDI cable is
attached to by clicking on Tools, then mouse over Port, and select the appropriate
serial port. If you only have one serial device attached, there will only be one
option. On my Ubuntu machine, it appears as /dev/ttyUSB0. Bear this in mind if you
have the XBee controller plugged into a USB port at the same time. I find it best to
have only one device plugged in to prevent mistakes.

*It is extremely important to get these settings right, because the code is compiled
for specific processors. Get the wrong one, and nothing will work.*

With all the settings correct, you can either click the upload button in the IDE to
compile and upload your code, or you can use the Ctrl + U key combination. A few
LEDs should start flashing for a few seconds as the code is uploading, and when they
stop, it's complete.


# Adjusting Haptic Feedback Signals

This is one place that will need some research to find the ideal signals to the
wearer of the glove.

Currently, there are 16 gestures defined in the glove software. Toward the end of
the `arduino/glove/pro_mini/pro_mini.ino` code, there is a long switch statement
with each of the gestures defined as a case. This is where the haptic feedback is
adjusted. Each motor mounted to the glove has an on or off state.
Digital pins for each motor are defined in the `Motors` enumeration. To start a 
motor vibrating, simply set its pin to `HIGH`, let it vibrate for a while by using
`delay()`, and then set its pin to `LOW` to stop the vibrating.
