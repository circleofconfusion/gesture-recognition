#include "xbeepacket.h"

XBeePacket::XBeePacket() {}

XBeeReceive::XBeeReceive() {}

bool XBeeReceive::isValid()
{
    union longConv {
        long l;
        quint8 b[8];
    };

    union shortConv {
        quint16 s;
        quint8 b[2];
    };

    longConv lc;
    shortConv sc;

    int total = type;

    lc.l = addr64;
    for (unsigned int i = 0; i < 8; ++i)
        total += lc.b[i];

    sc.s = addr16;
    total += sc.b[0];
    total += sc.b[1];

    total += options;

    for (int i = 0; i < data.length(); ++i)
        total += data[i];

    if ( (total & 0xFF) + checksum == 0xFF)
        return true;
    else
        return false;
}

XBeeTransmit::XBeeTransmit() {
    type = 0x10;
    messageId = 0;
    broadcastRadius = 0;
    options = 0;
}

quint8 XBeeTransmit::calculateChecksum()
{
    union longConv {
        long l;
        quint8 b[8];
    };

    union shortConv {
        quint16 s;
        quint8 b[2];
    };

    longConv lc;
    shortConv sc;

    int total = type;

    lc.l = addr64;
    for (unsigned int i = 0; i < 8; ++i)
        total += lc.b[i];

    sc.s = addr16;
    total += sc.b[0];
    total += sc.b[1];

    total += options;

    for (int i = 0; i < data.length(); ++i)
        total += data[i];

    return (0xFF - (total & 0xFF));
}

QByteArray XBeeTransmit::toByteArray()
{
    quint16 length = 14 + data.size();

    QByteArray message;
    // no point in resizing the underlying array every time we append!
    message.reserve(length + 3);
    message.append(0x7E);
    message.append(shortToByteArray(length));
    message.append(type);
    message.append(messageId);
    message.append(longToByteArray(addr64));
    message.append(shortToByteArray(addr16));
    message.append(byteToByteArray(broadcastRadius));
    message.append(byteToByteArray(options));

    QByteArray dqba;
    for (int i = 0; i < data.size(); ++i) {
        dqba.append(byteToByteArray(data[i]));
    }
    message.append(dqba);

    message.append(calculateChecksum());

    return message;
}

QByteArray XBeeTransmit::longToByteArray(long l)
{
    QByteArray qba;

    union longByte {
        long l;
        quint8 b[8];
    };

    longByte lb;
    lb.l = l;

    for (int i = 7; i >= 0; --i) {
        qba.append(byteToByteArray(lb.b[i]));
    }

    return qba;
}

QByteArray XBeeTransmit::shortToByteArray(quint16 s)
{
    QByteArray qba;

    union shortByte {
        quint16 s;
        quint8 b[2];
    };

    shortByte sb;
    sb.s = s;
    qba.append(byteToByteArray(sb.b[1]));
    qba.append(byteToByteArray(sb.b[0]));

    return qba;
}

QByteArray XBeeTransmit::byteToByteArray(quint8 b)
{
    QByteArray qba;

    if (b == 0x7E || b == 0x7D || b == 0x11 || b == 0x13) {
        qba.append(0x7D);
        qba.append( (b ^ 0x20) );
    }
    else {
        qba.append(b);
    }

    return qba;
}
