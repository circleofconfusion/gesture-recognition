#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T18:57:56
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets serialport

TARGET = GestureReader
TEMPLATE = app

# Use the appropriate path to your GRT installation
LIBS += -L"/usr/local/lib/" -lgrt

SOURCES += main.cpp\
    mainwindow.cpp \
    sensor.cpp \
    xbeepacket.cpp \
    datamanager.cpp \
    gesturepicker.cpp \
    sensordisplay.cpp \
    gesturecombobox.cpp \
    xbeeserial.cpp

HEADERS  += mainwindow.h \
    serialportcombobox.h \
    sensor.h \
    xbeepacket.h \
    datamanager.h \
    gesturepicker.h \
    enumgestures.h \
    sensordisplay.h \
    enumsensorlocation.h \
    gesturecombobox.h \
    xbeeserial.h

FORMS    += mainwindow.ui

CONFIG += c++11
