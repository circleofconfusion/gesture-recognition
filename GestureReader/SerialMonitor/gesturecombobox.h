#ifndef GESTURECOMBOBOX_H
#define GESTURECOMBOBOX_H

#include <QComboBox>
#include "enumgestures.h"

class GestureComboBox : public QComboBox
{
public:
    GestureComboBox();
};

#endif // GESTURECOMBOBOX_H
