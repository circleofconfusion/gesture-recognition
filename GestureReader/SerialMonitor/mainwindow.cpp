#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serialportcombobox.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // Serial port selectors.
    ui->setupUi(this);
    ui->leftSideCombo->clear();
    ui->leftSideCombo->blockSignals(true); // apparently, inserting an item triggers a signal
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->leftSideCombo->insertItem(0, info.portName());
    }
    ui->leftSideCombo->setCurrentIndex(-1);
    ui->leftSideCombo->blockSignals(false); // now that we're all inserted and set up, resume signals
    ui->leftSideCombo->show();

    //==========================================================================================
    // Status Tab
    //==========================================================================================

    char lh[2] = { 'L', 'H' };
    _lhSensorDisplay = new HandSensorDisplay(lh, ui->lhGroup);
    _lhSensorDisplay->disable();

    _serial1.setBaudRate(QSerialPort::Baud57600);
    _xBeeReadWrite1 = new XBeeSerial(&_serial1);
    _serial2.setBaudRate(QSerialPort::Baud57600);
    _xBeeReadWrite2 = new XBeeSerial(&_serial2);

    //==========================================================================================
    // Training Tab
    //==========================================================================================

    _trainingDP = new TrainingDataProcessor();
    for (unsigned int i = 0; i < (sizeof(GESTURE_STRINGS)/sizeof(*GESTURE_STRINGS)); ++i) {
        ui->cmbTrainingGesture->addItem(GESTURE_STRINGS[i]);
    }

    //==========================================================================================
    // Results Tab
    //==========================================================================================

    ui->AnbcResults->setColumnWidth(0, 200);
    ui->AnbcResults->setColumnWidth(1, 200);


    ui->SvmResults->setColumnWidth(0, 200);
    ui->SvmResults->setColumnWidth(1, 200);

    //==========================================================================================
    // Real-Time Monitoring Tab
    //==========================================================================================

    _anbcDP = new AnbcDataProcessor();
    _anbcDP->setNullRejectionCoefficient(ui->spinnerAnbcNullRejectionCoefficient->value());

    _svmDP = new SvmDataProcessor();

    // Send gesture tab
    ui->tableGesture->setColumnCount(4);
    ui->tableGesture->setRowCount((sizeof(GESTURE_STRINGS)/sizeof(*GESTURE_STRINGS)) - 1);
    ui->tableGesture->setColumnWidth(0, 100);
    ui->tableGesture->setColumnWidth(1, 150);
    ui->tableGesture->setColumnWidth(2, 150);
    ui->tableGesture->setColumnWidth(3, 150);
    ui->tableGesture->setHorizontalHeaderItem(0, new QTableWidgetItem("Gesture"));
    ui->tableGesture->setHorizontalHeaderItem(1, new QTableWidgetItem("Guess 1"));
    ui->tableGesture->setHorizontalHeaderItem(2, new QTableWidgetItem("Guess 2"));
    ui->tableGesture->setHorizontalHeaderItem(3, new QTableWidgetItem("Guess 3"));
    QSignalMapper* buttonMapper = new QSignalMapper(this);
    for (unsigned int i = 1; i < (sizeof(GESTURE_STRINGS)/sizeof(*GESTURE_STRINGS)); ++i) {
        QPushButton* button = new QPushButton(GESTURE_STRINGS[i]);
        connect(button, SIGNAL(clicked()), buttonMapper, SLOT(map()));
        buttonMapper->setMapping(button, i);
        ui->tableGesture->setCellWidget(i-1, 0, button);
    }
    connect(buttonMapper, SIGNAL(mapped(int)), this, SLOT(sendGesture(int)));
    for (int i = 0; i < ui->tableGesture->rowCount(); ++i) {
        ui->tableGesture->setCellWidget(i, 1, new GestureComboBox());
        ui->tableGesture->setCellWidget(i, 2, new GestureComboBox());
        ui->tableGesture->setCellWidget(i, 3, new GestureComboBox());
    }


    // handle menu actions

    // serial data events
    connect(ui->leftSideCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(setupSerial1(int)));
    connect(ui->leftSideCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(enableDisableButtons(int)));
    connect(_xBeeReadWrite1, SIGNAL(packetComplete(XBeePacket)), this, SLOT(updateSensorStatus(XBeePacket)));
    connect(_xBeeReadWrite2, SIGNAL(packetComplete(XBeePacket)), this, SLOT(updateSensorStatus(XBeePacket)));

    // status ui events
    QSignalMapper* signalMapper = new QSignalMapper(this);

    // training ui events
    connect(ui->cmbTrainingGesture, SIGNAL(currentIndexChanged(int)), this, SLOT(setGestureId(int)));
    connect(ui->playButton, SIGNAL(clicked(bool)), this, SLOT(startSerial()));
    connect(ui->stopButton, SIGNAL(clicked(bool)), this, SLOT(stopSerial()));
    connect(ui->btnRecordTrainingData, SIGNAL(clicked(bool)), this, SLOT(toggleTraining()));
    connect(ui->btnStopTrainingData, SIGNAL(clicked(bool)), this, SLOT(toggleTraining()));
    connect(ui->btnStopTrainingData, SIGNAL(clicked(bool)), this, SLOT(saveTrainingData()));
    connect(ui->btnTrainingFileSet, SIGNAL(clicked(bool)), this, SLOT(setTrainingFile()));
    connect(ui->textTrainingFileName, SIGNAL(textChanged()), this, SLOT(enableTrainingRecord()));
    connect(ui->textTrainingName, SIGNAL(textChanged()), this, SLOT(enableTrainingRecord()));
    connect(ui->textTrainingDesc, SIGNAL(textChanged()), this, SLOT(enableTrainingRecord()));

    // ANBC events
    connect(ui->btnChooseAnbcTrainingFile, SIGNAL(clicked(bool)), this, SLOT(setAnbcTrainingFile()));
    connect(ui->spinnerAnbcNullRejectionCoefficient, SIGNAL(valueChanged(double)), _anbcDP, SLOT(setNullRejectionCoefficient(double)));
    connect(ui->btnChooseAnbcModelFile, SIGNAL(clicked(bool)), this, SLOT(setAnbcModelFile()));
    connect(ui->btnSaveAnbcModelFile, SIGNAL(clicked(bool)), this, SLOT(saveAnbcModelFile()));
    connect(ui->btnStartListenAnbc, SIGNAL(clicked(bool)), this, SLOT(anbcStartListen()));
    connect(ui->btnStopListenAnbc, SIGNAL(clicked(bool)), this, SLOT(anbcStopListen()));
    connect(_anbcDP, SIGNAL(prediction(int)), this, SLOT(appendAnbcPrediction(int)));

    // SVM events
    connect(ui->btnChooseSvmTrainingFile, SIGNAL(clicked(bool)), this, SLOT(setSvmTrainingFile()));
    connect(ui->btnChooseSvmModelFile, SIGNAL(clicked(bool)), this, SLOT(setSvmModelFile()));
    connect(ui->btnSaveSvmModelFile, SIGNAL(clicked(bool)), this, SLOT(saveSvmModelFile()));
    connect(ui->btnStartListenSvm, SIGNAL(clicked(bool)), this, SLOT(svmStartListen()));
    connect(ui->btnStopListenSvm, SIGNAL(clicked(bool)), this, SLOT(svmStopListen()));
    connect(ui->cmbSvmKernelType, SIGNAL(currentIndexChanged(int)), this, SLOT(setSvmKernelType()));
    connect(ui->cmbSvmSvmType, SIGNAL(currentIndexChanged(int)), this, SLOT(setSvmKernelType()));
    connect(ui->checkSvmAutoGamma, SIGNAL(clicked(bool)), this, SLOT(toggleSvmAutoGamma(bool)));
    connect(ui->spinSvmGamma, SIGNAL(valueChanged(double)), _svmDP, SLOT(setGamma(double)));
    connect(ui->spinSvmC, SIGNAL(valueChanged(double)), _svmDP, SLOT(setC(double)));
    connect(ui->spinSvmDegree, SIGNAL(valueChanged(int)), _svmDP, SLOT(setDegree(int)));
    connect(ui->spinSvmCoef0, SIGNAL(valueChanged(double)), _svmDP, SLOT(setCoef0(double)));
    connect(ui->spinSvmNu, SIGNAL(valueChanged(double)), _svmDP, SLOT(setNu(double)));
    connect(_svmDP, SIGNAL(prediction(int)), this, SLOT(appendSvmPrediction(int)));

    // Gesture events
    connect(ui->btnChooseGestureOutput, SIGNAL(clicked(bool)), this, SLOT(setGestureCsv()));
    connect(ui->btnSaveGestureOutput, SIGNAL(clicked(bool)), this, SLOT(saveGestureCsv()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _trainingDP;
    delete _anbcDP;
}


void MainWindow::enableDisableButtons(int index)
{
    if (index > -1)
    {
        this->ui->playButton->setEnabled(true);
        this->ui->stopButton->setEnabled(true);
    }
    else
    {
        this->ui->playButton->setEnabled(false);
        this->ui->stopButton->setEnabled(false);
    }
}

void MainWindow::updateSensorStatus(XBeePacket packet)
{
    HandSensor lhSensor(packet.data);
    _lhSensorDisplay->update(lhSensor);
    _centralDM.addHandData(lhSensor);
}

void MainWindow::setupSerial1(int index)
{
    _serial1.setPortName(ui->leftSideCombo->currentText());
}

void MainWindow::startSerial()
{
    _serial1.open(QSerialPort::ReadWrite);
    _serial2.open(QSerialPort::ReadWrite);
    ui->playButton->setChecked(true);
    ui->stopButton->setChecked(false);
}

void MainWindow::stopSerial()
{
    _serial1.close();
    _serial2.close();
    ui->playButton->setChecked(false);
    ui->stopButton->setChecked(true);
}

void MainWindow::toggleTraining()
{
    qDebug() << "Recording for gesture: " << GESTURE_STRINGS[ui->cmbTrainingGesture->currentIndex()];
    // start recording
    if (ui->btnRecordTrainingData->isEnabled())
        _centralDM.addProcessor(_trainingDP);

    // stop recording
    if (ui->btnStopTrainingData->isEnabled())
        _centralDM.removeProcessor(_trainingDP);

    // enable/disable buttons
    ui->btnRecordTrainingData->setDisabled(ui->btnRecordTrainingData->isEnabled());
    ui->btnStopTrainingData->setDisabled(ui->btnStopTrainingData->isEnabled());
}

void MainWindow::setGestureId(int index)
{
    _trainingDP->setGestureId(index);
}

void MainWindow::setTrainingFile()
{
    ui->textTrainingFileName->setPlainText(QFileDialog::getSaveFileName(
                                               this,
                                               tr("Open Training File"),
                                               QDir::homePath(),
                                               "GRT File (*.grt)"
                                               ));
}

void MainWindow::enableTrainingRecord()
{
    if (ui->textTrainingFileName->toPlainText().length() < 1) return;
    if (ui->textTrainingName->toPlainText().length() < 1) return;
    if (ui->textTrainingDesc->toPlainText().length() < 1) return;

    ui->btnRecordTrainingData->setEnabled(true);
}

void MainWindow::saveTrainingData()
{
    _trainingDP->saveData(ui->textTrainingFileName->toPlainText(), ui->textTrainingName->toPlainText(), ui->textTrainingDesc->toPlainText());
}

void MainWindow::setAnbcTrainingFile()
{
    ui->textAnbcTrainingFileName->setPlainText(QFileDialog::getOpenFileName(
                                               this,
                                               tr("Open ANBC Training File"),
                                               QDir::homePath(),
                                               "GRT File (*.grt)"
                                               ));
    QString modelFileName = ui->textAnbcTrainingFileName->toPlainText().replace(QString(".grt"),QString("_ANBC_model.grt"));
    if(modelFileName.length() > 0) {
        ui->textAnbcModelFileName->setPlainText(modelFileName);
        ui->btnChooseAnbcModelFile->setEnabled(true);
        ui->btnSaveAnbcModelFile->setEnabled(true);
    }
}

void MainWindow::setAnbcModelFile()
{
    ui->textAnbcModelFileName->setPlainText(QFileDialog::getSaveFileName(
                                               this,
                                               tr("Open ANBC Model File"),
                                               ui->textAnbcModelFileName->toPlainText(),
                                               "GRT File (*.grt)"
                                               ));
}

void MainWindow::saveAnbcModelFile()
{
    // train
    _anbcDP->createModel(
                ui->textAnbcTrainingFileName->toPlainText(),
                ui->textAnbcModelFileName->toPlainText()
            );

    ui->btnStartListenAnbc->setEnabled(true);
}

void MainWindow::anbcStartListen()
{
    _centralDM.addProcessor(_anbcDP);
    ui->btnStartListenAnbc->setEnabled(false);
    ui->btnStopListenAnbc->setEnabled(true);
}

void MainWindow::anbcStopListen()
{
    _centralDM.removeProcessor(_anbcDP);
    ui->btnStartListenAnbc->setEnabled(true);
    ui->btnStopListenAnbc->setEnabled(false);
}

void MainWindow::appendAnbcPrediction(int prediction)
{
    int rowCount = ui->AnbcResults->rowCount();
    ui->AnbcResults->insertRow(rowCount);
    ui->AnbcResults->setCellWidget(rowCount, 0, new QLabel(GESTURE_STRINGS[prediction]));
    ui->AnbcResults->setCellWidget(rowCount, 1, new GestureComboBox());
}

void MainWindow::setSvmTrainingFile()
{
    ui->textSvmTrainingFileName->setPlainText(QFileDialog::getOpenFileName(
                                               this,
                                               tr("Open SVM Training File"),
                                               QDir::homePath(),
                                               "GRT File (*.grt)"
                                               ));
    QString modelFileName = ui->textSvmTrainingFileName->toPlainText().replace(QString(".grt"),QString("_" + ui->cmbSvmKernelType->currentText() + "_" + ui->cmbSvmSvmType->currentText() + "_SVM_model.grt"));
    if(modelFileName.length() > 0) {
        ui->textSvmModelFileName->setPlainText(modelFileName);
        ui->btnChooseSvmModelFile->setEnabled(true);
        ui->btnSaveSvmModelFile->setEnabled(true);
    }
}

void MainWindow::setSvmModelFile()
{
    ui->textSvmModelFileName->setPlainText(QFileDialog::getSaveFileName(
                                               this,
                                               tr("Open SVM Model File"),
                                               ui->textSvmModelFileName->toPlainText(),
                                               "GRT File (*.grt)"
                                               ));
}

void MainWindow::saveSvmModelFile()
{
    // train
    _svmDP->createModel(
                ui->textSvmTrainingFileName->toPlainText(),
                ui->textSvmModelFileName->toPlainText()
            );

    ui->btnStartListenSvm->setEnabled(true);
}

void MainWindow::svmStartListen()
{
    _centralDM.addProcessor(_svmDP);
    ui->btnStartListenSvm->setEnabled(false);
    ui->btnStopListenSvm->setEnabled(true);
}

void MainWindow::svmStopListen()
{
    _centralDM.removeProcessor(_svmDP);
    ui->btnStartListenSvm->setEnabled(true);
    ui->btnStopListenSvm->setEnabled(false);
}

void MainWindow::setSvmKernelType()
{
    _svmDP->setKernelType(ui->cmbSvmKernelType->currentIndex());
    _svmDP->setSvmType(ui->cmbSvmSvmType->currentIndex());

    // enable/disable UI components as necessary

    // disable all SVM ui parameters, and selectively enable
    ui->spinSvmC->setDisabled(true);
    ui->spinSvmCoef0->setDisabled(true);
    ui->spinSvmDegree->setDisabled(true);
    ui->spinSvmGamma->setDisabled(true);
    ui->spinSvmNu->setDisabled(true);

    if (!ui->checkSvmAutoGamma->isChecked()) ui->spinSvmGamma->setEnabled(true);

    // polynomial kernel enables degree and coef 0
    if (ui->cmbSvmKernelType->currentText() == "Poly") {
        ui->spinSvmDegree->setEnabled(true);
        ui->spinSvmCoef0->setEnabled(true);
    }

    // sigmoid kernel enables coef 0
    if (ui->cmbSvmKernelType->currentText() == "Sigmoid") {
        ui->spinSvmCoef0->setEnabled(true);
    }

    if (ui->cmbSvmSvmType->currentText() == "C SVC") {
        ui->spinSvmC->setEnabled(true);
    }

    if (ui->cmbSvmSvmType->currentText() == "Nu SVC") {
        ui->spinSvmNu->setEnabled(true);
    }
}

void MainWindow::toggleSvmAutoGamma(bool autoGamma)
{
    _svmDP->setAutoGamma(autoGamma);
    setSvmKernelType();
}

void MainWindow::appendSvmPrediction(int prediction)
{
    int rowCount = ui->SvmResults->rowCount();
    ui->SvmResults->insertRow(rowCount);
    ui->SvmResults->setCellWidget(rowCount, 0, new QLabel(GESTURE_STRINGS[prediction]));
    ui->SvmResults->setCellWidget(rowCount, 1, new GestureComboBox());
}

void MainWindow::sendGesture(int gesture)
{
    XBeeTransmit gesturePacket;
    gesturePacket.addr16 = 0xFFFE;
    // TODO figure out a way to not hard code this.
    gesturePacket.addr64 = 0x0013A20040E99CFC;

    switch(gesture) {

    case ONE:
        gesturePacket.data.append(0x01);
        _xBeeReadWrite1->write(gesturePacket.toByteArray());
        break;


    case TWO:
        gesturePacket.data.append(0x02);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case THREE:
        gesturePacket.data.append(0x03);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case FOUR:
        gesturePacket.data.append(0x04);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case FIVE:
        gesturePacket.data.append(0x05);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case SIX:
        gesturePacket.data.append(0x06);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case SEVEN:
        gesturePacket.data.append(0x07);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case EIGHT:
        gesturePacket.data.append(0x08);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case NINE:
        gesturePacket.data.append(0x09);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case TEN:
        gesturePacket.data.append(0x0A);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case OK:
        gesturePacket.data.append(0x0B);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case FREEZE:
        gesturePacket.data.append(0x0C);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case I_HEAR:
        gesturePacket.data.append(0x0D);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case I_SEE:
        gesturePacket.data.append(0x0E);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case ENEMY:
        gesturePacket.data.append(0x0F);
        _serial1.write(gesturePacket.toByteArray());
        break;

    case HOSTAGE:
        gesturePacket.data.append(0x11);
        _serial1.write(gesturePacket.toByteArray());
        break;
    }
}

void MainWindow::setGestureCsv()
{
    ui->textGestureFileName->setPlainText(QFileDialog::getSaveFileName(
                                               this,
                                               tr("Open gesture CSV file"),
                                               ui->textGestureFileName->toPlainText(),
                                               "CSV File (*.csv)"
                                               ));
}

void MainWindow::saveGestureCsv()
{
    QFile outfile(ui->textGestureFileName->toPlainText());
    if (outfile.open(QFile::WriteOnly|QFile::Truncate)) {
        QTextStream outStream(&outfile);
        outStream << "gesture,guess1,guess2,guess3" << "\n";

        for (unsigned int r = 0; r < ui->tableGesture->rowCount(); ++r) {
            QPushButton* button = (QPushButton*) ui->tableGesture->cellWidget(r, 0);
            QComboBox* box1 = (QComboBox*) ui->tableGesture->cellWidget(r,1);
            QComboBox* box2 = (QComboBox*) ui->tableGesture->cellWidget(r,2);
            QComboBox* box3 = (QComboBox*) ui->tableGesture->cellWidget(r,3);
            outStream << (r+1) << "," << box1->currentIndex() << "," << box2->currentIndex() << "," << box3->currentIndex() << "\n";
        }

        outfile.close();
    }
}
