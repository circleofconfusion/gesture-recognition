#include "datamanager.h"
#include "enumgestures.h"
#include <QFileDialog>

DataManager::DataManager(QObject* parent) : QObject(parent)
{
    // initialize queue enablers to be false, to be set to true later on from the GUI
    _lhQEnabled = false;
    _rhQEnabled = false;
    _leQEnabled = false;
    _reQEnabled = false;
    _lsQEnabled = false;
    _rsQEnabled = false;
}

void DataManager::addHandData(HandSensor& data)
{

    // enqueue the incoming data in the appropriate queue
    char* location = data.getLocation();
    GRT::VectorFloat vec;
    vec = data.toVector();
    switch (location[0])
    {
        case 'L':
            _lhQ.enqueue(vec);
            break;

        case 'R':
            _rhQ.enqueue(vec);
            break;

        default:
            qDebug() << data.getLocation();
            break;
    }

    sendData();
}

void DataManager::addArmData(ArmSensor& data)
{

    // enqueue the incoming data in the appropriate queue
    char* location = data.getLocation();
    GRT::VectorFloat vec;
    vec = data.toVector();
    switch (location[0])
    {
        case 'L':
            switch(location[1])
            {
                case 'E':
                    _leQ.enqueue(vec);
                    break;
                case 'S':
                    _lsQ.enqueue(vec);
                    break;
            }
            break;

        case 'R':
            switch(location[1])
            {
                case 'E':
                    _reQ.enqueue(vec);
                    break;
                case 'S':
                    _rsQ.enqueue(vec);
                    break;
            }
            break;
        default:
            qDebug() << data.getLocation();
    }

    sendData();
}

void DataManager::sendData()
{

    // If all queues have data, bundle up the heads into a full sample
    if ( ( !_lhQ.isEmpty() || !_lhQEnabled ) &&
         ( !_leQ.isEmpty() || !_leQEnabled ) &&
         ( !_lsQ.isEmpty() || !_lsQEnabled ) &&
         ( !_rhQ.isEmpty() || !_rhQEnabled ) &&
         ( !_reQ.isEmpty() || !_reQEnabled ) &&
         ( !_rsQ.isEmpty() || !_rsQEnabled ) )
    {
        // create and populate the sample vector
        //GRT::VectorFloat sample(64);

        // figure out how big the sample size will be
        int sampleSize = 0;
        if (_lhQEnabled) sampleSize += 14;
        if (_rhQEnabled) sampleSize += 14;
        if (_leQEnabled) sampleSize += 9;
        if (_reQEnabled) sampleSize += 9;
        if (_lsQEnabled) sampleSize += 9;
        if (_rsQEnabled) sampleSize += 9;

        // Create the sample vector and reserve some space
        GRT::VectorFloat sample;
        sample.reserve(sampleSize);

        GRT::VectorFloat lh;
        GRT::VectorFloat le;
        GRT::VectorFloat ls;
        GRT::VectorFloat rh;
        GRT::VectorFloat re;
        GRT::VectorFloat rs;

        // dequeue and insert as appropriate
        if (_lhQEnabled) {
            lh = _lhQ.dequeue();
            sample.insert(sample.end(), lh.begin(), lh.end());
        }
        if (_rhQEnabled) {
            rh = _rhQ.dequeue();
            sample.insert(sample.end(), rh.begin(), rh.end());
        }
        if (_leQEnabled) {
            le = _leQ.dequeue();
            sample.insert(sample.end(), le.begin(), le.end());
        }
        if (_reQEnabled) {
            re = _reQ.dequeue();
            sample.insert(sample.end(), re.begin(), re.end());
        }
        if (_lsQEnabled) {
            ls = _lsQ.dequeue();
            sample.insert(sample.end(), ls.begin(), ls.end());
        }
        if (_rsQEnabled) {
            rs = _rsQ.dequeue();
            sample.insert(sample.end(), rs.begin(), rs.end());
        }

        // Broadcast this sample to all the registered processors.
        for (unsigned i = 0; i < _dataProcessors.size(); ++i)
            _dataProcessors[i]->processSample(sample);
    }
}

void DataManager::addProcessor(DataProcessor* dp)
{
    // check to see if its already in the list
    for (unsigned int i = 0; i < _dataProcessors.size(); ++i) {
        if (_dataProcessors[i] == dp) return;
    }

    // got this far, so add processor to list
    _dataProcessors.push_back(dp);
}

void DataManager::removeProcessor(DataProcessor* dp)
{
    // search for data processor
    for (unsigned int i = 0; i < _dataProcessors.size(); ++i) {
        if (_dataProcessors[i] == dp) {
            _dataProcessors.erase(_dataProcessors.begin() + i);
        }
    }
}

void DataManager::listen(bool listen)
{
    if(listen) _listening = true;
    else _listening = false;
}

TrainingDataProcessor::TrainingDataProcessor()
{
    _trainingData.setNumDimensions(14); // 5 fingers + 9 DOF razor
    _trainingData.addClass(Gestures::ONE, "One");
    _trainingData.addClass(Gestures::TWO, "Two");
    _trainingData.addClass(Gestures::THREE, "Three");
    _trainingData.addClass(Gestures::FOUR, "FOUR");
    _trainingData.addClass(Gestures::FIVE, "Five");
    _trainingData.addClass(Gestures::SIX, "Six");
    _trainingData.addClass(Gestures::SEVEN, "Seven");
    _trainingData.addClass(Gestures::EIGHT, "Eight");
    _trainingData.addClass(Gestures::NINE, "Nine");
    _trainingData.addClass(Gestures::TEN, "Ten");
    _trainingData.addClass(Gestures::OK, "OK");
    _trainingData.addClass(Gestures::FREEZE, "Freeze");
    _trainingData.addClass(Gestures::I_HEAR, "I_hear");
    _trainingData.addClass(Gestures::I_SEE, "I_see");
    _trainingData.addClass(Gestures::ENEMY, "Enemy");
    _trainingData.addClass(Gestures::HOSTAGE, "Hostage");
    _gestureId = 1; // Set default to 1, which will be the first element in the combo box.
}

void TrainingDataProcessor::processSample(GRT::VectorFloat sample)
{
    _trainingData.addSample(_gestureId, sample);
}

void TrainingDataProcessor::saveData(QString fileName, QString datasetName, QString datasetDesc)
{
    _trainingData.setDatasetName(datasetName.toStdString());
    _trainingData.setInfoText(datasetDesc.toStdString());
    _trainingData.save(fileName.toStdString());
}

void TrainingDataProcessor::setGestureId(int id)
{
    _gestureId = id;
}

void TrainingDataProcessor::setDimensions(int dimensions)
{
    _trainingData.setNumDimensions(dimensions);
}

void TrainingDataProcessor::enable()
{

}

void TrainingDataProcessor::disable()
{

}

AnbcDataProcessor::AnbcDataProcessor()
{
    _anbc.enableScaling(true);
    _anbc.enableNullRejection(true);
}

void AnbcDataProcessor::setNullRejectionCoefficient(double coeff)
{
    _anbc.setNullRejectionCoeff(coeff);
}

void AnbcDataProcessor::createModel(QString trainingFileName, QString modelFileName)
{
    if (!_classificationData.loadDatasetFromFile(trainingFileName.toStdString())) {
        qDebug() << "Failed to load ANBC training data from file: " << trainingFileName;
    }

    if (!_anbc.train(_classificationData)) {
        qDebug() << "Failed to train ANBC.";
    }

    if (!_anbc.saveModelToFile(modelFileName.toStdString())) {
        qDebug() << "Failed to save ANBC model file: " << modelFileName;
    }

    if (!_anbc.loadModelFromFile(modelFileName.toStdString())) {
        qDebug() << "Failed to load ANBC model file: " << modelFileName;
    }
}

void AnbcDataProcessor::processSample(GRT::VectorFloat sample)
{

    if(_anbc.predict(sample)) {
        int classLabel = _anbc.getPredictedClassLabel();
        qDebug() << "ANBC Prediction: " << classLabel;
        emit(prediction(classLabel));
    }
    else {
        qDebug() << "ANBC failed to make a prediction.";
    }

}

void AnbcDataProcessor::enable()
{

}

void AnbcDataProcessor::disable()
{

}

SvmDataProcessor::SvmDataProcessor()
{
    _svm.enableScaling(true);
    _svm.enableNullRejection(true);
}

void SvmDataProcessor::createModel(QString trainingFileName, QString modelFileName)
{
    if (!_classificationData.loadDatasetFromFile(trainingFileName.toStdString())) {
        qDebug() << "Failed to load SVM training data from file: " << trainingFileName;
    }

    if (!_svm.train(_classificationData)) {
        qDebug() << "Failed to train SVM.";
    }

    if (!_svm.saveModelToFile(modelFileName.toStdString())) {
        qDebug() << "Failed to save SVM model file: " << modelFileName;
    }

    if (!_svm.loadModelFromFile(modelFileName.toStdString())) {
        qDebug() << "Failed to load SVM model file: " << modelFileName;
    }
}

void SvmDataProcessor::processSample(GRT::VectorFloat sample)
{
    if(_svm.predict(sample)) {
        int classLabel = _svm.getPredictedClassLabel();
        qDebug() << "SVM Prediction: " << classLabel;
        emit(prediction(classLabel));
    }
    else {
        qDebug() << "SVM failed to make a prediction.";
    }
}

void SvmDataProcessor::disable()
{

}

void SvmDataProcessor::enable()
{

}

void SvmDataProcessor::setKernelType(int index)
{
    _svm.setKernelType(index);
}

void SvmDataProcessor::setSvmType(int index)
{
    _svm.setSVMType(index);
}

void SvmDataProcessor::setAutoGamma(bool autoGamma)
{
    _svm.enableAutoGamma(autoGamma);
}

void SvmDataProcessor::setGamma(double gamma)
{
    _svm.setGamma(gamma);
}

void SvmDataProcessor::setC(double c)
{
    _svm.setC(c);
}

void SvmDataProcessor::setDegree(int degree)
{
    _svm.setDegree(degree);
}

void SvmDataProcessor::setCoef0(double coef0)
{
    _svm.setCoef0(coef0);
}

void SvmDataProcessor::setNu(double nu)
{
    _svm.setNu(nu);
}
