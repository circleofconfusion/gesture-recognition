#include "gesturepicker.h"

GesturePicker::GesturePicker(QWidget *parent) : QWidget(parent)
{
    // When adding a new gesture, make sure it's in the same place in both of these lists.
    _imageFiles << "../GestureImages/Freeze.png"
                << "../GestureImages/Hostage.png"
                << "../GestureImages/I_Dont_Understand.png"
                << "../GestureImages/Listen.png"
                << "../GestureImages/Look.png"
                << "../GestureImages/OK.png"
                << "../GestureImages/Sniper.png";

    _gestures << "Freeze"
              << "Hostage"
              << "I Don't Understand"
              << "Listen"
              << "Look"
              << "OK"
              << "Sniper";

    // populate the combo box
    for(int i = 0; i < _gestures.length(); ++i)
        _comboBox->addItem(_gestures[i]);

    _comboBox->setCurrentIndex(-1);

    // set up the image label
    _gestureImageLabel->setGeometry(0,35,120,120);
    _gestureImageLabel->setFrameShape(QFrame::Box);

    // set up signals and slots
    connect(_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setGesture(int)));
}

void GesturePicker::setGesture(int index)
{
    QPixmap pixmap(_imageFiles[index]);
    _gestureImageLabel->setPixmap(pixmap);
}
