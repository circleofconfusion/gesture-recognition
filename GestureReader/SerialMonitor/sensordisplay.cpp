#include "sensordisplay.h"

SensorDisplay::SensorDisplay(char * loc, QWidget * parent)
{
    QFont font;
    font.setPointSize(10);

    // define all the labels common to both layouts
    _location[0] = loc[0];
    _location[1] = loc[1];
    _grid = new QGridLayout(parent);
    _accelLbl = new QLabel("Accelerometer");
    _accelLbl->setFont(font);
    _accelLblX = new QLabel("-");
    _accelLblX->setFont(font);
    _accelLblY = new QLabel("-");
    _accelLblY->setFont(font);
    _accelLblZ = new QLabel("-");
    _accelLblZ->setFont(font);
    _magLbl = new QLabel("Magnetometer");
    _magLbl->setFont(font);
    _magLblX = new QLabel("-");
    _magLblX->setFont(font);
    _magLblY = new QLabel("-");
    _magLblY->setFont(font);
    _magLblZ = new QLabel("-");
    _magLblZ->setFont(font);
    _gyroLbl = new QLabel("Gyroscope");
    _gyroLbl->setFont(font);
    _gyroLblX = new QLabel("-");
    _gyroLblX->setFont(font);
    _gyroLblY = new QLabel("-");
    _gyroLblY->setFont(font);
    _gyroLblZ = new QLabel("-");
    _gyroLblZ->setFont(font);
}

char* SensorDisplay::getLocation()
{
    return _location;
}

HandSensorDisplay::HandSensorDisplay(char * loc, QWidget * parent): SensorDisplay::SensorDisplay(loc, parent)
{
    QFont font;
    font.setPointSize(10);

    // define all the labels specific to the HandSensor
    _fingersLbl = new QLabel("Fingers");
    _fingersLbl->setFont(font);
    _thumbLbl = new QLabel("-");
    _thumbLbl->setFont(font);
    _indexLbl = new QLabel("-");
    _indexLbl->setFont(font);
    _middleLbl = new QLabel("-");
    _middleLbl->setFont(font);
    _ringLbl = new QLabel("-");
    _ringLbl->setFont(font);
    _pinkyLbl = new QLabel("-");
    _pinkyLbl->setFont(font);

    // add labels to grid
    _grid->addWidget(_fingersLbl, 0, 0);
    _grid->addWidget(_thumbLbl, 0, 1);
    _grid->addWidget(_indexLbl, 0, 2);
    _grid->addWidget(_middleLbl, 0, 3);
    _grid->addWidget(_ringLbl, 0, 4);
    _grid->addWidget(_pinkyLbl, 0, 5);
    _grid->addWidget(_accelLbl, 1, 0);
    _grid->addWidget(_accelLblX, 1, 1);
    _grid->addWidget(_accelLblY, 1, 2);
    _grid->addWidget(_accelLblZ, 1, 3);
    _grid->addWidget(_magLbl, 2, 0);
    _grid->addWidget(_magLblX, 2, 1);
    _grid->addWidget(_magLblY, 2, 2);
    _grid->addWidget(_magLblZ, 2, 3);
    _grid->addWidget(_gyroLbl, 3, 0);
    _grid->addWidget(_gyroLblX, 3, 1);
    _grid->addWidget(_gyroLblY, 3, 2);
    _grid->addWidget(_gyroLblZ, 3, 3);
}

void HandSensorDisplay::update(HandSensor& sensor)
{
    float* acc = sensor.getAccelerometer();
    float* mag = sensor.getMagnetometer();
    float* gyr = sensor.getGyroscope();
    float* fingers = sensor.getFingers();

    // update the labels
    _accelLblX->setNum(acc[0]);
    _accelLblY->setNum(acc[1]);
    _accelLblZ->setNum(acc[2]);
    _magLblX->setNum(mag[0]);
    _magLblY->setNum(mag[1]);
    _magLblZ->setNum(mag[2]);
    _gyroLblX->setNum(gyr[0]);
    _gyroLblY->setNum(gyr[1]);
    _gyroLblZ->setNum(gyr[2]);
    _thumbLbl->setNum(fingers[0]);
    _indexLbl->setNum(fingers[1]);
    _middleLbl->setNum(fingers[2]);
    _ringLbl->setNum(fingers[3]);
    _pinkyLbl->setNum(fingers[4]);
}

void HandSensorDisplay::disable() {
    _accelLbl->setDisabled(true);
    _accelLblX->setDisabled(true);
    _accelLblY->setDisabled(true);
    _accelLblZ->setDisabled(true);
    _magLbl->setDisabled(true);
    _magLblX->setDisabled(true);
    _magLblY->setDisabled(true);
    _magLblZ->setDisabled(true);
    _gyroLbl->setDisabled(true);
    _gyroLblX->setDisabled(true);
    _gyroLblY->setDisabled(true);
    _gyroLblZ->setDisabled(true);
    _fingersLbl->setDisabled(true);
    _thumbLbl->setDisabled(true);
    _indexLbl->setDisabled(true);
    _middleLbl->setDisabled(true);
    _ringLbl->setDisabled(true);
    _pinkyLbl->setDisabled(true);
}

void HandSensorDisplay::enable()
{
    _accelLbl->setDisabled(false);
    _accelLblX->setDisabled(false);
    _accelLblY->setDisabled(false);
    _accelLblZ->setDisabled(false);
    _magLbl->setDisabled(false);
    _magLblX->setDisabled(false);
    _magLblY->setDisabled(false);
    _magLblZ->setDisabled(false);
    _gyroLbl->setDisabled(false);
    _gyroLblX->setDisabled(false);
    _gyroLblY->setDisabled(false);
    _gyroLblZ->setDisabled(false);
    _fingersLbl->setDisabled(false);
    _thumbLbl->setDisabled(false);
    _indexLbl->setDisabled(false);
    _middleLbl->setDisabled(false);
    _ringLbl->setDisabled(false);
    _pinkyLbl->setDisabled(false);
}

ArmSensorDisplay::ArmSensorDisplay(char * loc, QWidget * parent): SensorDisplay::SensorDisplay(loc, parent)
{
    // add labels to grid
    _grid->addWidget(_accelLbl, 0, 0);
    _grid->addWidget(_accelLblX, 0, 1);
    _grid->addWidget(_accelLblY, 0, 2);
    _grid->addWidget(_accelLblZ, 0, 3);
    _grid->addWidget(_magLbl, 1, 0);
    _grid->addWidget(_magLblX, 1, 1);
    _grid->addWidget(_magLblY, 1, 2);
    _grid->addWidget(_magLblZ, 1, 3);
    _grid->addWidget(_gyroLbl, 2, 0);
    _grid->addWidget(_gyroLblX, 2, 1);
    _grid->addWidget(_gyroLblY, 2, 2);
    _grid->addWidget(_gyroLblZ, 2, 3);
}

void ArmSensorDisplay::update(ArmSensor& sensor)
{
    float* acc = sensor.getAccelerometer();
    float* mag = sensor.getMagnetometer();
    float* gyr = sensor.getGyroscope();

    // update the labels
    _accelLblX->setNum(acc[0]);
    _accelLblY->setNum(acc[1]);
    _accelLblZ->setNum(acc[2]);
    _magLblX->setNum(mag[0]);
    _magLblY->setNum(mag[1]);
    _magLblZ->setNum(mag[2]);
    _gyroLblX->setNum(gyr[0]);
    _gyroLblY->setNum(gyr[1]);
    _gyroLblZ->setNum(gyr[2]);
}

void ArmSensorDisplay::disable()
{
    _accelLbl->setDisabled(true);
    _accelLblX->setDisabled(true);
    _accelLblY->setDisabled(true);
    _accelLblZ->setDisabled(true);
    _magLbl->setDisabled(true);
    _magLblX->setDisabled(true);
    _magLblY->setDisabled(true);
    _magLblZ->setDisabled(true);
    _gyroLbl->setDisabled(true);
    _gyroLblX->setDisabled(true);
    _gyroLblY->setDisabled(true);
    _gyroLblZ->setDisabled(true);
}

void ArmSensorDisplay::enable()
{
    _accelLbl->setDisabled(false);
    _accelLblX->setDisabled(false);
    _accelLblY->setDisabled(false);
    _accelLblZ->setDisabled(false);
    _magLbl->setDisabled(false);
    _magLblX->setDisabled(false);
    _magLblY->setDisabled(false);
    _magLblZ->setDisabled(false);
    _gyroLbl->setDisabled(false);
    _gyroLblX->setDisabled(false);
    _gyroLblY->setDisabled(false);
    _gyroLblZ->setDisabled(false);
}
