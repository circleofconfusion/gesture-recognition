#ifndef SERIALPORTCOMBOBOX_H
#define SERIALPORTCOMBOBOX_H

#include <QtGui>
#include <QComboBox>

class SerialPortComboBox : public QComboBox
{
Q_OBJECT
public:

    SerialPortComboBox(QWidget* parent):QComboBox(parent)
    {
        this->setParent(parent);
        connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(setSerialPort(int)));
    }
    ~ SerialPortComboBox(){}

public slots:
    void setSerialPort(int index)
    {
        qDebug() << "setSerialPort: " << this->currentText();
    }
};

#endif // SERIALPORTCOMBOBOX_H
