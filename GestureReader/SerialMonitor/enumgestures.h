#ifndef ENUMGESTURES_H
#define ENUMGESTURES_H

#include <QString>

enum Gestures {
    NULL_GESTURE = 0,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    OK,
    FREEZE,
    I_HEAR,
    I_SEE,
    ENEMY,
    HOSTAGE
};

// Human-readable string representations of the gestures.
// Use the above enum to get its corresponding string:
// GESTURE_STRINGS[THREE]
const QString GESTURE_STRINGS[] = {
    " ",
    "One",
    "Two",
    "Three",
    "Four",
    "Five",
    "Six",
    "Seven",
    "Eight",
    "Nine",
    "Ten",
    "OK",
    "Freeze",
    "I Hear",
    "I See",
    "Enemy",
    "Hostage"
};

#endif // ENUMGESTURES_H
