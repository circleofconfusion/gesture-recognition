#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include <QQueue>
#include <vector>
#include <GRT/GRT.h>
#include "sensor.h"
#include "enumsensorlocation.h"


class DataProcessor : public QObject
{
    Q_OBJECT

protected:
    bool _lhEnabled;
    bool _rhEnabled;
    bool _leEnabled;
    bool _reEnabled;
    bool _lsEnabled;
    bool _rsEnabled;

public:
    virtual void processSample(GRT::VectorFloat sample) =0;
    virtual void disable() =0;
    virtual void enable() =0;
};

class TrainingDataProcessor: public DataProcessor
{
    Q_OBJECT

    int _gestureId;

private:
    GRT::ClassificationData _trainingData;

public:
    TrainingDataProcessor();
    void processSample(GRT::VectorFloat sample);
    void setGestureId(int id);
    void saveData(QString fileName, QString datasetName, QString datasetDesc);

    /**
     * @brief setDimensions Sets the number of dimensions used in training data.
     * @param dimensions
     */
    void setDimensions(int dimensions);

    /**
     * @brief disable Disables data processing.
     */
    void disable();

    /**
     * @brief enable Enables data processing.
     */
    void enable();

};

class AnbcDataProcessor: public DataProcessor
{
    Q_OBJECT

private:
    GRT::ClassificationData _classificationData;
    GRT::ANBC _anbc;

public:
    AnbcDataProcessor();
    /**
     * Saves the model file and loads into the ANBC classifier.
     * @brief saveModelFile
     * @param fileName
     */
    bool saveModelFile(QString fileName);

    /**
     * Creates, saves, and loads model.
     * @brief createModel
     * @return
     */
    void createModel(QString trainingFileName, QString modelFileName);

    /**
     * Attempts to classify a sample.
     * @brief processSample
     * @param sample
     */
    void processSample(GRT::VectorFloat sample);

    /**
     * @brief disable Disables data processing.
     */
    void disable();

    /**
     * @brief enable Enables data processing.
     */
    void enable();

public slots:
    /**
     * Sets the null rejection coefficient for ANBC.
     * @brief setNullRejectionCoefficient
     * @param coeff
     */
    void setNullRejectionCoefficient(double coeff);

signals:
   int prediction(int prediction);

}; // end AnbcDataProcessor

class SvmDataProcessor: public DataProcessor
{
    Q_OBJECT

private:
    GRT::ClassificationData _classificationData;
    GRT::SVM _svm;

public:
    /** Constructor */
    SvmDataProcessor();

    /**
     * @brief Saves the model file and loads into the SVM classifier.
     * @param fileName
     * @return
     */
    bool saveModelFile(QString fileName);

    /**
     * @brief Creates, saves, and loads the model.
     * @param trainingFileName
     * @param modelFileName
     */
    void createModel(QString trainingFileName, QString modelFileName);

    /**
     * @brief Attempts to classify a sample.
     * @param sample
     */
    void processSample(GRT::VectorFloat sample);

    /**
     * @brief disable Disables data processing.
     */
    void disable();

    /**
     * @brief enable Enables data processing.
     */
    void enable();

public slots:

    /**
     * @brief Sets the SVM kernel type. Indices in UI match up with SVMKernelTypes enum.
     * @param index
     */
    void setKernelType(int index);

    /**
     * @brief Sets the SVM type. Indices in UI match up with SVMTypes enum.
     * @param index
     */
    void setSvmType(int index);
    void setAutoGamma(bool autoGamma);
    void setGamma(double gamma);
    void setC(double c);
    void setDegree(int degree);
    void setCoef0(double coef0);
    void setNu(double nu);

signals:
   int prediction(int prediction);

};

class DataManager : public QObject
{
    Q_OBJECT

private:
    QQueue<GRT::VectorFloat> _lhQ;
    bool _lhQEnabled;
    QQueue<GRT::VectorFloat> _rhQ;
    bool _rhQEnabled;
    QQueue<GRT::VectorFloat> _leQ;
    bool _leQEnabled;
    QQueue<GRT::VectorFloat> _reQ;
    bool _reQEnabled;
    QQueue<GRT::VectorFloat> _lsQ;
    bool _lsQEnabled;
    QQueue<GRT::VectorFloat> _rsQ;
    bool _rsQEnabled;
    bool _listening = false;
    std::vector<DataProcessor*> _dataProcessors;

    void sendData();

public:
    explicit DataManager(QObject *parent = 0);
    void addHandData(HandSensor& data);
    void addArmData(ArmSensor& data);

    /**
     * Turn listening to packets off or on.
     * @brief listen
     * @param listen Whether or not to keep listening
     */
    void listen(bool listen);

    /**
     * Adds a DataProcessor to the list of processors
     * this data manager will broadcast new samples to.
     * @brief addProcessor
     * @param dp
     */

    void addProcessor(DataProcessor* dp);
    /**
     * Removes a DataProcessor from the broadcast list.
     * @brief removeProcessor
     * @param dp
     */
    void removeProcessor(DataProcessor* dp);

signals:

public slots:

};

#endif // DATAMANAGER_H
