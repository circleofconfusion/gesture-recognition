#include "sensor.h"

Sensor::Sensor(char* loc)
{
    _location[0] = loc[0];
    _location[1] = loc[1];
}

Sensor::Sensor(QByteArray data)
{

}

Sensor::Sensor(Sensor& rhs)
{
    _location[0] = rhs._location[0];
    _location[1] = rhs._location[1];
    _dateTime = rhs._dateTime;
    _acc[0] = rhs._acc[0];
    _acc[1] = rhs._acc[1];
    _acc[2] = rhs._acc[2];
    _mag[0] = rhs._mag[0];
    _mag[1] = rhs._mag[1];
    _mag[2] = rhs._mag[2];
    _gyr[0] = rhs._gyr[0];
    _gyr[1] = rhs._gyr[1];
    _gyr[2] = rhs._gyr[2];
}

float* Sensor::getAccelerometer()
{
    return _acc;
}

float* Sensor::getMagnetometer()
{
    return _mag;
}

float* Sensor::getGyroscope()
{
    return _gyr;
}

HandSensor::HandSensor(char* loc): Sensor::Sensor(loc)
{

}

HandSensor::HandSensor(QByteArray data): Sensor::Sensor(data)
{
    update(data);
}

HandSensor::HandSensor(HandSensor& rhs): Sensor::Sensor(rhs)
{
    _finger[0] = rhs._finger[0];
    _finger[1] = rhs._finger[1];
    _finger[2] = rhs._finger[2];
    _finger[3] = rhs._finger[3];
    _finger[4] = rhs._finger[4];
}

HandSensor::~HandSensor()
{
    delete _acc;
    delete _mag;
    delete _gyr;
    delete _finger;
}

char* HandSensor::getLocation()
{
    return _location;
}

void HandSensor::update(QByteArray data)
{
    QDataStream ds(&data, QIODevice::ReadOnly);
    quint8 side;
    quint8 location;
    floatConv accX;
    floatConv accY;
    floatConv accZ;
    floatConv magX;
    floatConv magY;
    floatConv magZ;
    floatConv gyrX;
    floatConv gyrY;
    floatConv gyrZ;
    shortConv thumb;
    shortConv index;
    shortConv middle;
    shortConv ring;
    shortConv pinky;

    // extract the values from QByteArray
    ds >> side >> location
        >> accX.b[0] >> accX.b[1] >> accX.b[2] >> accX.b[3]
        >> accY.b[0] >> accY.b[1] >> accY.b[2] >> accY.b[3]
        >> accZ.b[0] >> accZ.b[1] >> accZ.b[2] >> accZ.b[3]
        >> magX.b[0] >> magX.b[1] >> magX.b[2] >> magX.b[3]
        >> magY.b[0] >> magY.b[1] >> magY.b[2] >> magY.b[3]
        >> magZ.b[0] >> magZ.b[1] >> magZ.b[2] >> magZ.b[3]
        >> gyrX.b[0] >> gyrX.b[1] >> gyrX.b[2] >> gyrX.b[3]
        >> gyrY.b[0] >> gyrY.b[1] >> gyrY.b[2] >> gyrY.b[3]
        >> gyrZ.b[0] >> gyrZ.b[1] >> gyrZ.b[2] >> gyrZ.b[3]
        >> thumb.b[0] >> thumb.b[1]
        >> index.b[0] >> index.b[1]
        >> middle.b[0] >> middle.b[1]
        >> ring.b[0] >> ring.b[1]
        >> pinky.b[0] >> pinky.b[1];

    // update stored values
    _location[0] = side;
    _location[1] = location;
    _acc[0] = accX.f;
    _acc[1] = accY.f;
    _acc[2] = accZ.f;
    _mag[0] = magX.f;
    _mag[1] = magY.f;
    _mag[2] = magZ.f;
    _gyr[0] = gyrX.f;
    _gyr[1] = gyrY.f;
    _gyr[2] = gyrZ.f;
    _finger[0] = thumb.s;
    _finger[1] = index.s;
    _finger[2] = middle.s;
    _finger[3] = ring.s;
    _finger[4] = pinky.s;
}

float* HandSensor::getFingers()
{
    return _finger;
}

GRT::VectorFloat HandSensor::toVector()
{
    GRT::VectorFloat data(14);
    data[0] = _acc[0];
    data[1] = _acc[1];
    data[2] = _acc[2];
    data[3] = _mag[0];
    data[4] = _acc[1];
    data[5] = _mag[2];
    data[6] = _gyr[0];
    data[7] = _gyr[0];
    data[8] = _gyr[0];
    data[9] = (float) _finger[0];
    data[10] = (float) _finger[1];
    data[11] = (float) _finger[2];
    data[12] = (float) _finger[3];
    data[13] = (float) _finger[4];

    return data;
}

ArmSensor::ArmSensor(char* loc): Sensor::Sensor(loc)
{

}

ArmSensor::ArmSensor(QByteArray data): Sensor::Sensor(data)
{
    update(data);
}

ArmSensor::ArmSensor(ArmSensor& rhs): Sensor::Sensor(rhs)
{

}

ArmSensor::~ArmSensor()
{
    delete _acc;
    delete _mag;
    delete _gyr;
}

char* ArmSensor::getLocation()
{
    return _location;
}

void ArmSensor::update(QByteArray data)
{
    QDataStream ds(data);
    quint8 side;
    quint8 location;
    floatConv accX;
    floatConv accY;
    floatConv accZ;
    floatConv magX;
    floatConv magY;
    floatConv magZ;
    floatConv gyrX;
    floatConv gyrY;
    floatConv gyrZ;

    // extract the values from the QByteArray
    ds >> side >> location
        >> accX.b[0] >> accX.b[1] >> accX.b[2] >> accX.b[3]
        >> accY.b[0] >> accY.b[1] >> accY.b[2] >> accY.b[3]
        >> accZ.b[0] >> accZ.b[1] >> accZ.b[2] >> accZ.b[3]
        >> magX.b[0] >> magX.b[1] >> magX.b[2] >> magX.b[3]
        >> magY.b[0] >> magY.b[1] >> magY.b[2] >> magY.b[3]
        >> magZ.b[0] >> magZ.b[1] >> magZ.b[2] >> magZ.b[3]
        >> gyrX.b[0] >> gyrX.b[1] >> gyrX.b[2] >> gyrX.b[3]
        >> gyrY.b[0] >> gyrY.b[1] >> gyrY.b[2] >> gyrY.b[3]
        >> gyrZ.b[0] >> gyrZ.b[1] >> gyrZ.b[2] >> gyrZ.b[3];

    // update stored values
    _location[0] = side;
    _location[1] = location;
    _acc[0] = accX.f;
    _acc[1] = accY.f;
    _acc[2] = accZ.f;
    _mag[0] = magX.f;
    _mag[1] = magY.f;
    _mag[2] = magZ.f;
    _gyr[0] = gyrX.f;
    _gyr[1] = gyrY.f;
    _gyr[2] = gyrZ.f;
}

GRT::VectorFloat ArmSensor::toVector()
{
    GRT::VectorFloat data(9);
    data[0] = _acc[0];
    data[1] = _acc[1];
    data[2] = _acc[2];
    data[3] = _mag[0];
    data[4] = _acc[1];
    data[5] = _mag[2];
    data[6] = _gyr[0];
    data[7] = _gyr[0];
    data[8] = _gyr[0];

    return data;
}
