#include "xbeeserial.h"

XBeeSerial::XBeeSerial(QSerialPort* serial, QObject * parent):
    QObject(parent), _serial(serial)
{
    connect(_serial, SIGNAL(readyRead()), this, SLOT(read()));
}

XBeeSerial::~XBeeSerial()
{
    if (_serial->isOpen())
        _serial->close();
}

void write(XBeeTransmit outPacket)
{
    QByteArray outBytes = outPacket.toByteArray();
}


void XBeeSerial::read()
{
    // check that we have plenty of bytes to work with
    if (_serial->bytesAvailable() < 128) {
        return;
    }

    // find the delimiter
    quint8 delim;
    do {
        delim = readByte();
    } while (delim != 0x7E);

    XBeeReceive packet;

    // read header data
    packet.length = readShort();
    packet.type = readByte();
    packet.addr64 = readLong();
    packet.addr16 = readShort();
    packet.options = readByte();

    // read data body
    // just pull in the bits based on the length minus the 12 bits already gathered
    for (int i = 0; i < ( packet.length - 12 ); ++i)
        packet.data.append(readByte());

    // get the checksum
    packet.checksum = readByte();\

    if (packet.isValid()) {
        emit packetComplete(packet);
    }
    else {
        qDebug() << "Dropping invalid packet";
    }
}


// We're using API with escaped characters. While the 0x7D escape characters make
// the apparent incoming data longer, they don't count against the length, so this scheme
// can be used to burn through the bits as if the escapes didn't exist.
quint8 XBeeSerial::readByte()
{
    // read in a byte
    QDataStream ds(_serial);
    quint8 b;
    ds >> b;

    // check to see if byte is an escape byte
    if (b == 0x7D) {
        // read in the next byte and xor with 0x20
        ds >> b;
        b = b xor 0x20;
    }

    return b;
}

quint16 XBeeSerial::readShort()
{

    quint8 b = readByte();
    quint16 s;
    s = b;
    s = s << 8;
    s += readByte();
    return s;
}

quint64 XBeeSerial::readLong()
{
    quint8 b = readByte();
    quint64 l = b;

    // read 8 bytes into the long
    for (unsigned int i = 0; i < 7; ++i) {
        l = l << 8;
        l += readByte();
    }

    return l;
}

float XBeeSerial::readFloat()
{
    union fconv {
        float f;
        quint8 b[4];
    };
    fconv data;
    for (unsigned int i = 0; i < 4; ++i)
        data.b[i] = readByte();

    return data.f;
}

void XBeeSerial::write(const QByteArray& data)
{
    qDebug() << data.toHex();
    _serial->write(data);
}
