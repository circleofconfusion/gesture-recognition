#ifndef SENSORDISPLAY_H
#define SENSORDISPLAY_H

#include "sensor.h"

class SensorDisplay
{
protected:
    char _location[2];

    // labels that make up the widget data
    QWidget * _parent;
    QGridLayout * _grid;
    QLabel * _accelLbl;
    QLabel * _accelLblX;
    QLabel * _accelLblY;
    QLabel * _accelLblZ;
    QLabel * _magLbl;
    QLabel * _magLblX;
    QLabel * _magLblY;
    QLabel * _magLblZ;
    QLabel * _gyroLbl;
    QLabel * _gyroLblX;
    QLabel * _gyroLblY;
    QLabel * _gyroLblZ;

public:
    SensorDisplay(char * loc, QWidget * parent);

    char* getLocation();
    virtual void disable() =0;
    virtual void enable() = 0;
};

class HandSensorDisplay: public SensorDisplay
{
private:
    quint16 _finger[5];
    QLabel * _fingersLbl;
    QLabel * _thumbLbl;
    QLabel * _indexLbl;
    QLabel * _middleLbl;
    QLabel * _ringLbl;
    QLabel * _pinkyLbl;

public:
    HandSensorDisplay(char* loc, QWidget * parent);

    void update(HandSensor& sensor);
    void enable();
    void disable();
};

class ArmSensorDisplay: public SensorDisplay
{
public:
    ArmSensorDisplay(char* loc, QWidget * parent);

    void update(ArmSensor &sensor);
    void enable();
    void disable();
};

#endif // SENSORDISPLAY_H
