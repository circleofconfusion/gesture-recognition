#include "gesturecombobox.h"

GestureComboBox::GestureComboBox()
{
    for (unsigned int i = 0; i < (sizeof(GESTURE_STRINGS)/sizeof(*GESTURE_STRINGS)); ++i) {
        this->addItem(GESTURE_STRINGS[i]);
    }
}
