#ifndef XBEEPACKET_H
#define XBEEPACKET_H

#include <QByteArray>

class XBeePacket
{
public:
    QByteArray data;
    quint8 type;
    quint64 addr64;
    quint16 addr16;
    quint8 options;

    XBeePacket();
};

class XBeeReceive: public XBeePacket
{
public:
    quint16 length;
    quint8 checksum;

    XBeeReceive();
    bool isValid();
};

class XBeeTransmit: public XBeePacket
{
public:
    // length and checksum are calculated values
    quint8 messageId;
    quint8 broadcastRadius;

    XBeeTransmit();
    quint8 calculateChecksum();

    QByteArray toByteArray();

private:
    QByteArray longToByteArray(long l);
    QByteArray shortToByteArray(quint16 s);
    QByteArray byteToByteArray(quint8 b);
};

#endif // XBEEPACKET_H
