#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "xbeepacket.h"
#include "xbeeserial.h"
#include "sensor.h"
#include "sensordisplay.h"
#include "datamanager.h"
#include "gesturepicker.h"
#include "enumsensorlocation.h"
#include "enumgestures.h"
#include "gesturecombobox.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setupSerial1(int index);
    void enableDisableButtons(int index);
    void toggleTraining();
    void updateSensorStatus(XBeePacket packet);
    void startSerial();
    void stopSerial();

    /** Sets the appropriate gesture ID in training data manager. */
    void setGestureId(int index);
    /** Sets the training file name into the training file field. */
    void setTrainingFile();
    /** Saves the training data */
    void saveTrainingData();
    /** Enables the training record button */
    void enableTrainingRecord();

    /** Sets the ANBC training file name into the ANBC training file field. */
    void setAnbcTrainingFile();

    /** Sets the ANBC model file name into the ANBC model file field. */
    void setAnbcModelFile();

    /** Saves the ANBC model file to the specified name. */
    void saveAnbcModelFile();

    void anbcStartListen();
    void anbcStopListen();
    void appendAnbcPrediction(int prediction);

    void svmStartListen();
    void svmStopListen();
    void saveSvmModelFile();
    void setSvmModelFile();
    void setSvmTrainingFile();
    void setSvmKernelType();
    void toggleSvmAutoGamma(bool autoGamma);
    void appendSvmPrediction(int prediction);


    void sendGesture(int gesture);
    void setGestureCsv();
    void saveGestureCsv();

private:
    QSerialPort _serial1;
    XBeeSerial* _xBeeReadWrite1;
    QSerialPort _serial2;
    XBeeSerial* _xBeeReadWrite2;
    Ui::MainWindow* ui;
    HandSensorDisplay* _lhSensorDisplay;
    ArmSensorDisplay* _leSensorDisplay;
    ArmSensorDisplay* _lsSensorDisplay;
    HandSensorDisplay* _rhSensorDisplay;
    ArmSensorDisplay* _reSensorDisplay;
    ArmSensorDisplay* _rsSensorDisplay;
    DataManager _centralDM;
    TrainingDataProcessor* _trainingDP;
    AnbcDataProcessor* _anbcDP;
    SvmDataProcessor* _svmDP;
};

#endif // MAINWINDOW_H
