#ifndef SENSOR_H
#define SENSOR_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QString>
#include <QDebug>
#include <QDateTime>
#include <GRT/GRT.h>

class Sensor
{
protected:
    char _location[2];
    QDateTime _dateTime;

    // sensor data
    float* _acc = new float[3];
    float* _mag = new float[3];
    float* _gyr = new float[3];

    union floatConv {
        float f;
        quint8 b[4];
    };

    union shortConv {
        quint16 s;
        quint8 b[2];
    };

public:
    Sensor(char* loc);
    Sensor(QByteArray data);
    Sensor(Sensor& rhs);

    virtual GRT::VectorFloat toVector() = 0;
    virtual void update(QByteArray data) = 0;
    float* getAccelerometer();
    float* getMagnetometer();
    float* getGyroscope();
};

class HandSensor:public Sensor
{
private:
    float* _finger = new float[5];

public:
    HandSensor(char* loc);
    HandSensor(QByteArray data);
    HandSensor(HandSensor& rhs);
    ~HandSensor();
    char* getLocation();
    GRT::VectorFloat toVector();
    void update(QByteArray data);
    float* getFingers();
};

class ArmSensor: public Sensor
{
public:
    ArmSensor(char* loc);
    ArmSensor(QByteArray data);
    ArmSensor(ArmSensor& rhs);
    ~ArmSensor();

    char* getLocation();
    GRT::VectorFloat toVector();
    void update(QByteArray data);
};

#endif // SENSOR_H
