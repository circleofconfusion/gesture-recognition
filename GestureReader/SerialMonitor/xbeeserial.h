#ifndef XBEESERIALREADER_H
#define XBEESERIALREADER_H

#include <QObject>
#include <QSerialPort>
#include <QDebug>
#include <QDataStream>
#include "xbeepacket.h"

class XBeeSerial: public QObject
{
    Q_OBJECT
public:
    XBeeSerial(QSerialPort *serial, QObject * parent = 0);
    ~XBeeSerial();

    void write(const QByteArray& data);

private:
    QSerialPort* _serial;

    // reads one byte
    // handles XBee api escape bytes by reading second byte xor 0x20
    quint8 readByte();

    // reads two bytes - most significant byte first
    quint16 readShort();

    // reads eight bytes - most significant byte first
    quint64 readLong();

    // reads four bytes into float - most significant byte first
    float readFloat();

signals:
    void packetComplete(XBeePacket packet);

private slots:
    void read();
};

#endif // XBEESERIALREADER_H
