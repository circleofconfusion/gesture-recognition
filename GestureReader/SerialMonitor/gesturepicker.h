#ifndef GESTUREPICKER_H
#define GESTUREPICKER_H

#include <QObject>
#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QPixmap>

class GesturePicker : public QWidget
{
    Q_OBJECT

private:
    QStringList _imageFiles;
    QStringList _gestures;
    QComboBox * _comboBox = new QComboBox(this);
    QLabel * _gestureImageLabel = new QLabel(this);

public:
    explicit GesturePicker(QWidget *parent = 0);

//signals:

//public slots:

private slots:
    void setGesture(int index);
};

#endif // GESTUREPICKER_H
